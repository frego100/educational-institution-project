<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notas', function (Blueprint $table) {
            //$table->smallInteger('id_reg_cur')->autoIncrement();
            $table->tinyIncrements('id_registro');
            $table->integer('dni_reg_alu');
            $table->integer('dni_reg_doc');
            $table->tinyInteger('id_reg_curso')->unsigned();
            //$table->Integer('dni_reg_alu')->unsigned();
            //$table->Integer('dni_reg_doc')->unsigned();
            //$table->Integer('id_reg_curso')->unsigned();
            $table->tinyInteger('nota');
            $table->timestamps();
            $table->softDeletes();
            
        });
        Schema::table('notas', function($table) {
            $table->foreign('dni_reg_alu')->references('dni')->on('alumnos');
            $table->foreign('dni_reg_doc')->references('dni')->on('docentes');
            $table->foreign('id_reg_curso')->references('id_curso')->on('cursos');
            //$table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notas');
    }
}
