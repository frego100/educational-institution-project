<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Docente;
use App\Alumno;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    
    //DOCENTE
    public function indexDocente(Request $request)
    {
        $docente = Docente::all();
        return $docente;
        //Esta función nos devolvera todas las tareas que tenemos en nuestra BD
    }
    
    public function saveDocente(Request $req)
    {
        //print_r($req->input());
        $user = new Docente;
        $user->name =$req->name;
        $user->first_name =$req->first_name;
        $user->last_name =$req->last_name;
        $user->dni =$req->dni;
        $user->date =$req->date;
        $user->status = true;
        $user->email =$req->email;
        $user->phone =$req->phone;
        $user->save();
        return view('home'); 
    }
    
    public function showDocente(Request $request)
    {
        $docente = Docente::findOrFail($request->dni);
        return $docente;
        //Esta función devolverá los datos de una tarea que hayamos seleccionado para cargar el formulario con sus datos
    }
    
    public function updateDocente(Request $request)
    {
        $docente = Docente::findOrFail($request->dni);

        $docente->name = $request->name;
        $docente->first_name = $request->first_name;
        $docente->last_name = $request->last_name;

        $docente->save();

        return $docente;
        //Esta función actualizará la tarea que hayamos seleccionado
    }
    
    public function destroy(Request $request)
    {
        $docente = Docente::destroy($request->dni);
        return $docente;
        //Esta función obtendra el dni de la tarea que hayamos seleccionado y la borrará de nuestra BD
    }
    
    // ALUMNO
    
    public function saveAlumno(Request $req)
    {
        //print_r($req->input());
        $user = new Alumno;
        $user->name =$req->name;
        $user->first_name =$req->first_name;
        $user->last_name =$req->last_name;
        $user->dni =$req->dni;
        $user->dni_father =$req->dni_father;
        $user->date =$req->date;
        $user->save(); 
        return view('home'); 
    }
}
