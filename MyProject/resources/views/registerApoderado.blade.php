@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registrar Apoderado</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('apoderados.registro') }}">
                        @csrf
                      <input
                        type="text"
                        name="name"
                        placeholder="Nombre"
                        class="form-control mb-2"
                      />
                      <input
                        type="text"
                        name="first_name"
                        placeholder="Primer Apellido"
                        class="form-control mb-2"
                      />
                      <input
                        type="text"
                        name="last_name"
                        placeholder="Segundo Apellido"
                        class="form-control mb-2"
                      />
                      <input
                        type="number"
                        name="dni"
                        placeholder="DNI"
                        class="form-control mb-2"
                      />
                      <input
                        type="text"
                        name="email"
                        placeholder="Correo Electronico"
                        class="form-control mb-2"
                      />
                      <input
                        type="number"
                        name="phone"
                        placeholder="Numero de Telefono"
                        class="form-control mb-2"
                      />
                      <button class="btn btn-primary btn-block" type="submit">Agregar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
