@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Notas del Alumno  </div>

                <div class="card-body">
                  <form method="POST" action="{{ route('notas.alumno') }}">
                    <section>
                    <table id="table">
                      <tr>
                        <td style="text-align: center;"><b>Codigo<b></td>
                        <td style="text-align: center;"><b>Curso<b></td>
                        <td style="text-align: center;"><b>Docente<b></td>
                        <td style="text-align: center;"><b>Nota<b></td>
                      </tr>
                      <tr>
                        <td>1</td>
                        <td>COMUNICACION</td>
                        <td>BARRIOS/OCHOA, DAYANA GERALDINE</td>
                        <td>
                        <table id="notasalumno">
                          <td> 10</td>
                          <td> 10</td>
                          <td> 10</td>
                          <td> 10</td>
                          <td> 10</td>
                        </table>

                        </td>
      

                      </tr>
                      <tr>
                        <td>2</td>
                        <td>MATEMATICA</td>
                        <td>BAUTISTA/LOPEZ, MIRIAM DEYSI</td>
                        <td>
                        <table id="notasalumno">
                          <td> 10</td>
                          <td> 10</td>
                          <td> 10</td>
                          <td> 10</td>
                          <td> 10</td>
                        </table>

                        </td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>FISICA</td>
                        <td>BEDREGAL/CARRAZCO, CIELO MARIELA</td>
                        <td>
                        <table id="notasalumno">
                          <td> 10</td>
                          <td> 10</td>
                          <td> 10</td>
                          <td> 10</td>
                          <td> 10</td>
                        </table>

                        </td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>QUIMICA</td>
                        <td>BELIZARIO/HUANACO, MARTIN RODRIGO</td>
                        <td>
                        <table id="notasalumno">
                          <td> 10</td>
                          <td> 10</td>
                          <td> 10</td>
                          <td> 10</td>
                          <td> 10</td>
                        </table>

                        </td>
                      </tr>
                      <tr>
                        <td>5</td>
                        <td>RELIGION</td>
                        <td>CABANA/JOVE, YOIS PAMELA</td>
                        <td>
                        <table id="notasalumno">
                          <td> 10</td>
                          <td> 10</td>
                          <td> 10</td>
                          <td> 10</td>
                          <td> 10</td>
                        </table>

                        </td>
                      </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection