@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registrar Alumno </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('alumnos.registro') }}">
                        @csrf
                      <input
                        type="text"
                        name="name"
                        placeholder="Nombre"
                        class="form-control mb-2"
                      />
                      <input
                        type="text"
                        name="first_name"
                        placeholder="Primer Apellido"
                        class="form-control mb-2"
                      />
                      <input
                        type="text"
                        name="last_name"
                        placeholder="Segundo Apellido"
                        class="form-control mb-2"
                      />
                      <input
                        type="date"
                        name="date"
                        placeholder="fecha de nacimiento"
                        class="form-control mb-2"
                      />
                      <input 
                        type="email" 
                        class="form-control @error('email') is-invalid @enderror" 
                        name="email" 
                        value="{{ old('email') }}" 
                        placeholder="E-mail del Alumno" 
                      />
                      <input
                        type="text"
                        name="name_apoderado"
                        placeholder="Nombre del apoderado"
                        class="form-control mb-2"
                      />
                      <input
                        type="number"
                        name="dni_father"
                        placeholder="DNI del apoderado"
                        min="0" max="10000000"
                      />
                      <input type="submit" value="Informacion del apoderado">

                      <input type="number" name="grado" placeholder= "Grado del Alumno" id="grado" class="form-control mb-2"/ min="0" max="10000000">
                      <input type="text" name="seccion" placeholder="Seccion del Alumno" class="form-control mb-2"/>


                      <a href="{{ route('notas.alumno') }}" type="submit" >Notas del alumno </a>
                      <button class="btn btn-primary btn-block" type="submit">Agregar</button>
                    </form> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
