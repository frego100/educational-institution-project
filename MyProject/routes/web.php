<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//---------PARA EL DOCENTE
Route::view('/home/Formulario_Docente','registerDocente')->name('docentes.form');
Route::post('/home/Registrar_Docente','HomeController@saveDocente')->name('docentes.registro');
Route::get('/tareas', 'HomeController@indexDocente');
Route::put('/tareas/actualizar', 'HomeController@updateDocente');
Route::delete('/tareas/borrar/{dni}', 'HomeController@destroyDocente');
Route::get('/tareas/buscar', 'HomeController@showDocente');


//---------PARA EL ALUMNO
Route::view('/home/Formulario_Alumno','registerAlumno')->name('alumnos.form');
Route::post('/home/Registrar_Alumno','HomeController@saveAlumno')->name('alumnos.registro');


// --------Notas del alumno
Route::view('/home/Notas_Alumno','notasalumno')->name('notas.alumno');

//---------PARA EL APODERADO
Route::view('/home/Formulario_Apoderado','registerApoderado')->name('apoderados.form');
Route::post('/home/Registrar_Apoderado','ApoderadoController@store')->name('apoderados.registro');

